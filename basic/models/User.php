<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $pwd
 * @property string $phone
 * @property string $name
 * @property string $last_name
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    public $password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function fields()
    {
        $fields = parent::fields();

        unset($fields['pwd'], $fields['auth_key']);

        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'match', 'pattern' => '#^[\w_-]+$#i'],
            ['username', 'unique', 'targetClass' => User::className(), 'message' => 'Такой пользователь уже существует.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['name', 'string', 'max' => 255],
            ['last_name', 'string', 'max' => 255],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            [['phone'], 'number', 'message'=>'{attribute} должен состоять только из цифр'],
            [['phone'], 'string', 'length' => 11, 'notEqual'=>'{attribute} должен содержать 11 цифр'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логин',
            'phone' => 'Телефон',
            'name' => 'Имя',
            'last_name' => 'Фамилия',
        ];
    }




    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }
 
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('findIdentityByAccessToken is not implemented.');
    }
 
    public function getId()
    {
        return $this->getPrimaryKey();
    }
 
    public function getAuthKey()
    {
        return $this->auth_key;
    }
 
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }


    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
 
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->pwd);
    }


    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->pwd = Yii::$app->security->generatePasswordHash($password);
    }
 
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
 
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->generateAuthKey();
            }

            if($this->password) {
                $this->setPassword($this->password);
            }

            return true;
        }
        return false;
    }
}
